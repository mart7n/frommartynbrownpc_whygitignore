Reset
SetType                 TYPE_PROPERTIES
SetPersistent           true
SetStartDelay           10
SetDuration             3600
AddRequirement          "DOWNLOAD_ASSET?cdn=2&path=bws3/trick_or_treat_package_release-4.3.x_2018-02-12_11_49.zip"
AddProperty             "LIVE_OPS" "{\"payload\":{\"progression\":\"main\", \"version\":0,\"time\":{\"start\":0,\"timePolicy\":0},\"conditions\":{\"min_teaser_level\":50,\"min_available_level\":6,\"max_available_level\":0},\"milestones\":[{\"target\":{\"offset\":0, \"amount\":25},\"rewards\":[{\"type\":61013,\"quantity\":1}]},{\"target\":{\"offset\":1, \"amount\":50},\"rewards\":[{\"type\":61013,\"quantity\":1}]},{\"target\":{\"offset\":2, \"amount\":75},\"rewards\":[{\"type\":61013,\"quantity\":1}]},{\"target\":{\"offset\":3, \"amount\":100},\"rewards\":[{\"type\":61013,\"quantity\":1}]},{\"target\":{\"offset\":4, \"amount\":150},\"rewards\":[{\"type\":61013,\"quantity\":1}]}]},\"type\":\"trick_or_treat\",\"id\":${RANDOM:1000000:1999999}}"
PushMessage             ${RANDOM:1000000:1999999} ${RANDOM:1000000:1999999} ${RANDOM:1000000:1999999}
