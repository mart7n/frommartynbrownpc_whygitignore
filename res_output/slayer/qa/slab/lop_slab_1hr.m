Reset
SetType                 TYPE_PROPERTIES
SetPersistent           true
SetStartDelay           10
SetDuration             3600
AddRequirement          "DOWNLOAD_ASSET?cdn=2&path=bws3/stellas_lab_package_release-4.3.x_2018-02-12_11_49.zip"
AddProperty             "LIVE_OPS" "{\"payload\":{\"progression\":\"liveops\",\"liveop_subtype\":\"rogue_shooter\",\"time\":{\"start\":0,\"timePolicy\":0},\"milestones\":[{\"narrative\":{\"dialog\":null},\"rewards\":[{\"quantity\":1,\"type\":61013}],\"target\":{\"offset\":6}},{\"narrative\":{\"dialog\":null},\"rewards\":[{\"quantity\":1,\"type\":61014}],\"target\":{\"offset\":12}},{\"narrative\":{\"dialog\":null},\"rewards\":[{\"quantity\":1,\"type\":61013}],\"target\":{\"offset\":18}}],\"conditions\":{\"min_available_level\":22,\"max_available_level\":0,\"min_teaser_level\":0},\"levels\":[46,47,48,49,50,51,52,53,54,55]},\"id\":${RANDOM:1000000:1999999},\"type\":\"stellas_lab\"}"
PushMessage             ${RANDOM:1000000:1999999} 100 ${RANDOM:1000000:1999999}
