Reset
SetType                 TYPE_PROPERTIES
SetPersistent           true
SetStartDelay           5
SetDuration             60
AddProperty             "LIVE_OPS" "{\"id\":${RANDOM:1000000:1999999},\"type\":\"broom_race\",\"payload\":{\"version\":0,\"time\":{\"start\":0,\"timePolicy\":0},\"conditions\":{\"min_available_level\":40,\"max_available_level\":0,\"min_teaser_level\":1,\"min_stars_left\":15},\"milestones\":[{\"target\":{\"offset\":0},\"rewards\":[{\"type\":61009,\"quantity\":1},{\"type\":61010,\"quantity\":50}]}]}}"
PushMessage             ${RANDOM:1000000:1999999} ${RANDOM:1000000:1999999} ${RANDOM:1000000:1999999}
