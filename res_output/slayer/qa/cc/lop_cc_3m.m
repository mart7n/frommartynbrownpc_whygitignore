Reset
SetType                 TYPE_PROPERTIES
SetPersistent           true
SetStartDelay           5
SetDuration             180
AddRequirement          "DOWNLOAD_ASSET?cdn=2&path=bws3/cauldron_classic_package.zip"
AddProperty             "LIVE_OPS" "{\"payload\":{\"progression\":\"liveops\",\"time\":{\"start\":0,\"timePolicy\":0},\"milestones\":[{\"narrative\":{\"dialog\":100102},\"rewards\":[{\"quantity\":1,\"type\":61003}],\"target\":{\"offset\":4}},{\"narrative\":{\"dialog\":100103},\"rewards\":[{\"quantity\":1,\"type\":61004}],\"target\":{\"offset\":7}},{\"narrative\":{\"dialog\":100104},\"rewards\":[{\"quantity\":1,\"type\":61009}],\"target\":{\"offset\":10}}],\"conditions\":{\"min_available_level\":35,\"max_available_level\":0,\"min_teaser_level\":22},\"levels\":[1,2,3,4,5,6,7,8,9,10],\"rewards\":[{\"quantity\":200,\"type\":61000},{\"quantity\":100,\"type\":61000},{\"quantity\":1,\"type\":61013},{\"quantity\":1,\"type\":61013}]},\"id\":${RANDOM:1000000:1999999},\"type\":\"cauldron_classic\"}"
PushMessage             ${RANDOM:1000000:1999999} ${RANDOM:1000000:1999999} ${RANDOM:1000000:1999999}
