Reset
SetType                 TYPE_PROPERTIES
SetPersistent           false
SetStartDelay           5
SetDuration             3600
AddRequirement          "DOWNLOAD_ASSET?cdn=2&path=bws3/treasure_cave_package_release-4.3.x_2018-02-12_11_49.zip"
AddProperty             "LIVE_OPS" "{\"payload\":{\"progression\":\"liveops\",\"time\":{\"start\":0,\"timePolicy\":0},\"milestones\":[{\"narrative\":{\"dialog\":100102},\"rewards\":[{\"quantity\":1,\"type\":61009}],\"target\":{\"offset\":4}},{\"narrative\":{\"dialog\":100103},\"rewards\":[{\"quantity\":1,\"type\":61006}],\"target\":{\"offset\":7}},{\"narrative\":{\"dialog\":100104},\"rewards\":[{\"quantity\":1,\"type\":61013}],\"target\":{\"offset\":10}}],\"conditions\":{\"min_available_level\":14,\"max_available_level\":0,\"min_teaser_level\":22},\"levels\":[16,17,18,19,20,21,22,23,24,25]},\"id\":${RANDOM:1000000:1999999},\"type\":\"treasure_stream\"}"
PushMessage             ${RANDOM:1000000:1999999} ${RANDOM:1000000:1999999} ${RANDOM:1000000:1999999}
